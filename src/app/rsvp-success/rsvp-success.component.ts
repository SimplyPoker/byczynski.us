import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rsvp-success',
  templateUrl: './rsvp-success.component.html',
  styleUrls: ['./rsvp-success.component.css'],
  styles: [':host { width: 100%; height: 100%; }']
})
export class RsvpSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
