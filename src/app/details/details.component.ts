import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  styles: [':host { width: 100%; height: 100%; }']
})
export class DetailsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
