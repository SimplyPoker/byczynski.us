// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB0gNHmRrgzyqHb1DN4ZQRlTlGpVLtaAjA",
    authDomain: "byczynski-wedding.firebaseapp.com",
    databaseURL: "https://byczynski-wedding.firebaseio.com",
    projectId: "byczynski-wedding",
    storageBucket: "byczynski-wedding.appspot.com",
    messagingSenderId: "302276584637",
    appId: "1:302276584637:web:dc5902c1efa4f2816a03d4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
